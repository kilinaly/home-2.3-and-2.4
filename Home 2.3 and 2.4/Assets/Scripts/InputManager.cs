using System;
using System.Collections;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static float HorizontalAxis;
    public static event Action<float> JumpAction;
    public static event Action<string> FireAction;

    [SerializeField] private Animator anim;

    private float jumpTimer;
    private Coroutine waitJumpCoroutine;
    private Coroutine waitEndThrowingCoroutine;
    private static readonly int IsRunning = Animator.StringToHash("IsRunning");

    private void Start()
    {
        HorizontalAxis = 0;
    }

    private void Update()
    {
        HorizontalAxis = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump"))
        {
            if (waitJumpCoroutine == null)
            {
                waitJumpCoroutine = StartCoroutine(WaitJump());
                return;
            }

            jumpTimer = Time.time;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            FireAction?.Invoke("Fire1");
        }
        
        if ((Input.GetButtonDown("Fire2")) && (waitEndThrowingCoroutine == null))
        {
                waitEndThrowingCoroutine = StartCoroutine(WaitThrowing());
                FireAction?.Invoke("Fire2");
                //return;
        }
    }

    private IEnumerator WaitThrowing()
    {
        anim.SetBool("isThrowing",true);
        yield return new WaitForSeconds(0.1f);
        anim.SetBool("isThrowing",false);
        waitEndThrowingCoroutine = null;
    }

    private IEnumerator WaitJump()
    {
        anim.SetBool("isJumping",true);
        yield return new WaitForSeconds(0.1f);
        anim.SetBool("isJumping",false);
        yield return new WaitForSeconds(0.1f);
        if (JumpAction != null)
        {
            var force = Time.time - jumpTimer <= 0.2f ? 1.25f : 1f;//TODO убрать в константы
            JumpAction.Invoke(force);
        }

        waitJumpCoroutine = null;
    }
}