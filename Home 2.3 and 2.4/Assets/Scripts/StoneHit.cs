﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using UnityEditor;
using UnityEngine;

public class StoneHit : MonoBehaviour
{
    [SerializeField] public GameObject player;
    private GameObject stone;
    
    public Player playerScript;
    
    private float sizePlayer;
    private void Start()
    {
        stone = this.gameObject;
        sizePlayer = player.GetComponent<BoxCollider2D>().size.y;
    }
    
    private void OnCollisionEnter2D (Collision2D collision)
    {
        if (collision.gameObject == player || collision.gameObject == stone)
        {
            print("Камень касается игрока");
            if (stone.transform.position.y >= player.transform.position.y+sizePlayer/2+0.15f)//чтобы урон наносился только если player под камнем
            {
                playerScript.Hit(5);
                print("Камень нанёс урон игроку"); 
                
                stone.SetActive(false); 
                print("Камень исчез");
            }
    }
    }
    
    private void Update()
    {

    }

}
