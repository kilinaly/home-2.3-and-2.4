using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour, IPlayer, IHitBox
{
    [SerializeField] private int health = 5;
    [SerializeField] private Animator animator;
    [SerializeField] public GameObject[] stones;
    
    private PlayerWeapon[] weapons;

    private Collider2D playerCollider2D;
    private Collider2D[] stonesColliders2D;


    public void RegisterPlayer()
    {
        GameManager manager = FindObjectOfType<GameManager>();

        if (manager.Player == null)
        {
            manager.Player = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        
    }

    private void Awake()
    {
        RegisterPlayer();
    }

    private void Start()
    {
        weapons = GetComponents<PlayerWeapon>();
        InputManager.FireAction += OnAttack;
        playerCollider2D = GetComponent<Collider2D>();
        for (int i = 0; i < stones.Length; i++)
        {
            stonesColliders2D[i] = GetComponent<Collider2D>();
        }
    }

    private void OnDestroy()
    {
        InputManager.FireAction -= OnAttack;
    }

    public int Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0f)
            {
                Die();
            }
        }
    }
    public void Hit(int damage)
    {
        Health -= damage;
    }

    public void Die()
    {
        print("Player is died");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    
    

    private void OnAttack(string axis)
    {
        foreach (var weapon in weapons)
        {
            if (weapon.Axis == axis)
            {
                weapon.SetDamage();
                animator.SetTrigger("Attack");
            }
        }
    }

    private void Update()
    {
        for (int i = 0; i < stones.Length; i++)
        {
            if (playerCollider2D.IsTouching(stonesColliders2D[i]))
            {
              Hit(1);
            } 
        }
        
    }
    
    
    
}