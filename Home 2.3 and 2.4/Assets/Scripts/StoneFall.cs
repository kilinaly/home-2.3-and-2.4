﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneFall : MonoBehaviour
{
    [SerializeField] public GameObject[] stones;
    [SerializeField] public GameObject[] points;

    private SpriteRenderer rend;
    
    private int randomTimeBetweenFallingStones;
    private Coroutine waitCoroutine;
    private Coroutine waitCoroutineFading;
    private  int stoneNumber=0;


    void Start()
    {
        StonesStartPosition();
        SetActiveTrueFalse(false);
    }
    
   private void Update()
    {
        
        if (stoneNumber < stones.Length)
        {
            randomTimeBetweenFallingStones = Random.Range(1, 4);

            if (waitCoroutine == null)
            {
                waitCoroutine = StartCoroutine(Wait(randomTimeBetweenFallingStones));
                return;
            }
        }
        
        if ((stoneNumber >= stones.Length)&&(stoneNumber < stones.Length*2))
        {
                if (waitCoroutineFading == null)
                {
                    waitCoroutineFading = StartCoroutine(Fading());
                    return;
                }
        }
        
        if (stoneNumber==stones.Length * 2)
        {
            StonesStartPosition();
            SetActiveTrueFalse(true);
            FadeInNow();
            stoneNumber = 0;
        }
        
    }
    
    private IEnumerator Wait(float time)
    {
        stones[stoneNumber].SetActive(true);
        yield return new WaitForSeconds(time);
        stoneNumber++;
        waitCoroutine = null;
    }

    private void SetActiveTrueFalse(bool b)
    {
        for (int j = 0; j < stones.Length; j++)
        {
            stones[j].SetActive(false);
        }
    }
    
    private void StonesStartPosition()
    {
        for (int j = 0; j < stones.Length; j++)
        {
            stones[j].transform.position = new Vector3(points[j].transform.position.x,points[j].transform.position.y, 0f);
        }
    }
    
    IEnumerator FadeOut()//исчезание 1го камня
    {
        for (float f = 1f; f >=-0.05; f-=0.05f)
        {
            rend = stones[stoneNumber-stones.Length].GetComponent<SpriteRenderer>();
            Color c = rend.material.color;
            c.a = f;
            rend.material.color = c;
            yield return new WaitForSeconds(0.01f);
            
        }
    }
    
    private void FadeInNow()//видимость всех камней сразу
    {
        for (int j = 0; j < stones.Length; j++)
        {
            rend = stones[j].GetComponent<SpriteRenderer>();
            Color c = rend.material.color;
            c.a = 1;
            rend.material.color = c;
        }
    }

    private IEnumerator Fading()//перебор камней для исчезания
    {
        StartCoroutine("FadeOut");
        yield return new WaitForSeconds(2f);
        stones[stoneNumber-stones.Length].SetActive(false);//чтобы камни не зависали в воздухе после исчезания соседнего камня
        stoneNumber++;
        waitCoroutineFading = null;
    }
    


}
